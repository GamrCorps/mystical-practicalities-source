package GamrCorps.mystical.network;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldSavedData;

public class ManaNetwork extends WorldSavedData{

    public int currentMana;

    public ManaNetwork(String par1Str)
    {
        super(par1Str);
        currentMana = 0;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbttagcompound)
    {
        currentMana = nbttagcompound.getInteger("currentMana");
    }

    @Override
    public void writeToNBT(NBTTagCompound nbttagcompound)
    {
        nbttagcompound.setInteger("currentMana", currentMana);
    }
}

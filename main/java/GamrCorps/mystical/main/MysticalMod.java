package GamrCorps.mystical.main;
//Mystical Mod Main File by GamrCorps(expertgamer16985) and Lost_Alien!

import GamrCorps.mystical.GUD.GUD;
import GamrCorps.mystical.block.ModBlocks;
import GamrCorps.mystical.commands.ManaCommand;
import GamrCorps.mystical.creativetab.MysticalCreativeTab;
import GamrCorps.mystical.handler.CentEventHandler;
import GamrCorps.mystical.handler.GuiHandler;
import GamrCorps.mystical.handler.HUDHandler;
import GamrCorps.mystical.item.ModItems;
import GamrCorps.mystical.lib.Constants;
import GamrCorps.mystical.proxy.CommonProxy;
import GamrCorps.mystical.recipes.ModRecipes;
import GamrCorps.mystical.recipes.RecipeHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Constants.MODID, version = Constants.VERSION, name = Constants.MODNAME)
public class MysticalMod {

    @SidedProxy(clientSide = "GamrCorps.mystical.proxy.ClientProxy", serverSide = "GamrCorps.mystical.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static CreativeTabs mysticalCreativeTab = new MysticalCreativeTab(CreativeTabs.getNextID(), "mysticalTab");

    public static CentEventHandler CentralHandler;

    public static Logger log = LogManager.getLogger("Mystical Practicalities");

    @Mod.Instance(Constants.MODID)
    public static MysticalMod instance;

    private static int modGuiIndex = 10;

    public static final int guiQuarry = modGuiIndex++;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        ModItems.init();
        ModItems.registerItems();
        ModBlocks.init();
        ModBlocks.registerBlocks();
        ModRecipes.registerCraftingRecipes();
        RecipeHandler.registerItems();
        CentralHandler = new CentEventHandler();
        proxy.registerTileEntities();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        //proxy.registerTileEntities();
        MinecraftForge.EVENT_BUS.register(new HUDHandler());
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        GUD.checkVersion();
    }

    @Mod.EventHandler
    public void serverLoad(FMLServerStartingEvent event)
    {
        event.registerServerCommand(new ManaCommand());
    }
}
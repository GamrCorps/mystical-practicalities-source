package GamrCorps.mystical.block;

import GamrCorps.mystical.gui.GUIS;
import GamrCorps.mystical.item.ModItems;
import GamrCorps.mystical.main.MysticalMod;
import GamrCorps.mystical.tileentity.TileEntityQuarry;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.Random;

public class BlockQuarry extends BlockContainer {
    //Chunkloader?
    //Create Box Area (client particle)
    protected Random field_149942_b = new Random();

    protected BlockQuarry() {
        super(Material.iron);

    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityQuarry();
    }

    @Override
    public boolean hasTileEntity(int metadata) {

        return true;
    }

    public boolean onBlockActivated(World world, int intx, int inty, int intz, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        TileEntityQuarry temp = (TileEntityQuarry) world.getTileEntity(intx, inty, intz);

        if (world.isRemote) {
            return true;
        } else if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().getItem() != null && player.inventory.getCurrentItem().getItem() == ModItems.itemMysticalGlass) {
            if (temp.getOwner() == null || temp.getOwner() == "") {
                temp.setOwner(player);
            }
            temp.sendChatInfoToPlayer(player);
            return true;
        } else {
//            TileEntityDispenser tileentitydispenser = (TileEntityDispenser) world.getTileEntity(intx, inty, intz);

            if (temp != null) {
                player.openGui(MysticalMod.instance, GUIS.QUARRY.ordinal(), world, intx, inty, intz);
            }

            return true;
        }
    }

    public void breakBlock(World p_149749_1_, int p_149749_2_, int p_149749_3_, int p_149749_4_, Block p_149749_5_, int p_149749_6_) {
        TileEntityQuarry tileentitydispenser = (TileEntityQuarry) p_149749_1_.getTileEntity(p_149749_2_, p_149749_3_, p_149749_4_);

        if (tileentitydispenser != null) {
            for (int i1 = 0; i1 < tileentitydispenser.getSizeInventory(); ++i1) {
                ItemStack itemstack = tileentitydispenser.getStackInSlot(i1);

                if (itemstack != null) {
                    float f = this.field_149942_b.nextFloat() * 0.8F + 0.1F;
                    float f1 = this.field_149942_b.nextFloat() * 0.8F + 0.1F;
                    float f2 = this.field_149942_b.nextFloat() * 0.8F + 0.1F;

                    while (itemstack.stackSize > 0) {
                        int j1 = this.field_149942_b.nextInt(21) + 10;

                        if (j1 > itemstack.stackSize) {
                            j1 = itemstack.stackSize;
                        }

                        itemstack.stackSize -= j1;
                        EntityItem entityitem = new EntityItem(p_149749_1_, (double) ((float) p_149749_2_ + f), (double) ((float) p_149749_3_ + f1), (double) ((float) p_149749_4_ + f2), new ItemStack(itemstack.getItem(), j1, itemstack.getItemDamage()));

                        if (itemstack.hasTagCompound()) {
                            entityitem.getEntityItem().setTagCompound((NBTTagCompound) itemstack.getTagCompound().copy());
                        }

                        float f3 = 0.05F;
                        entityitem.motionX = (double) ((float) this.field_149942_b.nextGaussian() * f3);
                        entityitem.motionY = (double) ((float) this.field_149942_b.nextGaussian() * f3 + 0.2F);
                        entityitem.motionZ = (double) ((float) this.field_149942_b.nextGaussian() * f3);
                        p_149749_1_.spawnEntityInWorld(entityitem);
                    }
                }
            }

            p_149749_1_.func_147453_f(p_149749_2_, p_149749_3_, p_149749_4_, p_149749_5_);
        }

        super.breakBlock(p_149749_1_, p_149749_2_, p_149749_3_, p_149749_4_, p_149749_5_, p_149749_6_);
    }
}

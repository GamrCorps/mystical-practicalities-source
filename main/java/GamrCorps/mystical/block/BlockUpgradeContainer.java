package GamrCorps.mystical.block;

import GamrCorps.mystical.item.ModItems;
import GamrCorps.mystical.recipes.RecipeHandler;
import GamrCorps.mystical.recipes.RecipeUpgradeCreation;
import GamrCorps.mystical.tileentity.TileEntityUpgradeContainer;
import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

public class BlockUpgradeContainer extends Block implements ITileEntityProvider {
    public BlockUpgradeContainer() {
        super(Material.iron);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta) {
        return new TileEntityUpgradeContainer();
    }

    @Override
    public boolean hasTileEntity(int metadata) {
        return true;
    }

    @Override
    public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer player, int l, float m, float n, float o) {
        TileEntityUpgradeContainer temp = (TileEntityUpgradeContainer) world.getTileEntity(i, j, k);
        ItemStack stack = player.getHeldItem();
        ItemStack stack1 = player.getItemInUse();
        ItemStack stack2 = player.getCurrentEquippedItem();
        if (world.isRemote) {
            return true;
        }
        /*
        else if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().getItem() !=null && player.inventory.getCurrentItem().getItem() == ModItems.itemUpgradePotentia){
            if (temp.hasUpgrade() == true && temp.getUpgrade() == "Potentia"){
                temp.incAmount(10);
            }
            else if (temp.hasUpgrade() == false){
                temp.setUpgrade("Potentia", 10);
            }
            else if (temp.hasUpgrade() == true && temp.getUpgrade() != "Potentia"){
                player.addChatMessage(new ChatComponentText("This Container does not contain Potentia!"));
            }
            return true;
        }
        */
        else {
            for (RecipeUpgradeCreation recipe : RecipeHandler.upgradeCreationRecipes) {
                if (recipe.matches(stack) || recipe.matches(stack1) || recipe.matches(stack2)) {
                    if (temp.hasUpgrade() == true && temp.getUpgrade() == recipe.getName()) {
                        temp.incAmount(recipe.getAmount());
                    } else if (temp.hasUpgrade() == false) {
                        temp.setUpgrade(recipe.getName(), recipe.getAmount());
                    } else if (temp.hasUpgrade() == true && temp.getUpgrade() != recipe.getName()) {
                        player.addChatMessage(new ChatComponentText("This Container does not contain " + recipe.getName() + "!"));
                    }
                    return true;
                }
            }
            if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().getItem() != null && player.inventory.getCurrentItem().getItem() == ModItems.itemMysticalGlass) {
                temp.sendChatInfoToPlayer(player);
                return true;
            }
            return true;
        }
    }
}

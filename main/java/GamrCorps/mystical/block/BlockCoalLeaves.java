package GamrCorps.mystical.block;


import GamrCorps.mystical.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.Random;


public class BlockCoalLeaves extends Block
{

    public BlockCoalLeaves()
    {
        super(Material.leaves);
       this.setCreativeTab(CreativeTabs.tabBlock);
       this.setResistance(7.0F);
       this.setHardness(0.2F);
       this.setLightOpacity(1);
       this.setStepSound(soundTypeGrass);
       this.setBlockName(Constants.nameBlockCoalLeaves);
       this.setBlockTextureName(Constants.MODID + ":coal_leaves");
    }


    public boolean isOpaqueCube()
    {
        return false;
    }
    public int quantityDropped(Random xRandom)
    {
        return xRandom.nextInt(4) == 0 ? 1 : 0;
    }

    public Item getItemDropped(int x, Random y, int z)
    {
        return Item.getItemFromBlock(ModBlocks.coalSapling);
    }



    @Override
        public void onBlockDestroyedByPlayer(World world, int x, int y, int z, int c)
        {

            EntityPlayer player = Minecraft.getMinecraft().thePlayer;
            if (!world.isRemote) {
                if (player.getCurrentEquippedItem().getItem() == Items.shears && player.getCurrentEquippedItem().getItem() != null) {
                    EntityItem ei = new EntityItem(world, x, y, z, new ItemStack(Item.getItemFromBlock(ModBlocks.coalLeaves)));
                    world.spawnEntityInWorld(ei);
                }
            }
    }
}
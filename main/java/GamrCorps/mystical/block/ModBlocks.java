package GamrCorps.mystical.block;

import GamrCorps.mystical.lib.Constants;
import GamrCorps.mystical.main.MysticalMod;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

public class ModBlocks {
    public static Block mutatedSapling;
    public static Block mutatedLog;
    public static Block mutatedLeaves;
    public static Block mutatedStone;
    public static Block coalSapling;
    public static Block coalLeaves;
    public static Block quarry;
    public static Block upgradeContainer;
    public static Block manaFurnaceGen;


    public static void init() {
        mutatedSapling = new BlockMutatedSapling().setCreativeTab(MysticalMod.mysticalCreativeTab);
        mutatedLog = new BlockMutatedLog().setCreativeTab(MysticalMod.mysticalCreativeTab);
        mutatedLeaves = new BlockMutatedLeaves().setCreativeTab(MysticalMod.mysticalCreativeTab);
        mutatedStone = new BlockMutatedStone().setCreativeTab(MysticalMod.mysticalCreativeTab);
        coalSapling = new BlockCoalSapling().setCreativeTab(MysticalMod.mysticalCreativeTab);
        coalLeaves = new BlockCoalLeaves().setCreativeTab(MysticalMod.mysticalCreativeTab);
        quarry = new BlockQuarry().setCreativeTab(MysticalMod.mysticalCreativeTab);
        upgradeContainer = new BlockUpgradeContainer().setCreativeTab(MysticalMod.mysticalCreativeTab);
        manaFurnaceGen = new BlockManaFurnaceGeneration().setCreativeTab(MysticalMod.mysticalCreativeTab);
    }

    public static void registerBlocks() {
        GameRegistry.registerBlock(mutatedSapling, Constants.nameMutatedSapling);
        GameRegistry.registerBlock(mutatedLog, Constants.nameMutatedLog);
        GameRegistry.registerBlock(mutatedLeaves, Constants.nameMutatedLeaves);
        GameRegistry.registerBlock(mutatedStone, Constants.nameMutatedStone);
        GameRegistry.registerBlock(coalSapling, Constants.nameBlockCoalSapling);
        GameRegistry.registerBlock(coalLeaves, Constants.nameBlockCoalLeaves);
        GameRegistry.registerBlock(quarry, Constants.nameQuarry);
        GameRegistry.registerBlock(upgradeContainer, Constants.nameUpgradeContainer);
        GameRegistry.registerBlock(manaFurnaceGen, Constants.nameFurnaceGen);
    }
}
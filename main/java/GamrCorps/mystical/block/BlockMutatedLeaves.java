package GamrCorps.mystical.block;

import GamrCorps.mystical.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockMutatedLeaves extends Block {
    public BlockMutatedLeaves() {
        super(Material.leaves);
        this.setBlockName(Constants.nameMutatedLeaves);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setBlockTextureName(Constants.MODID + ":oak_leaves");
        this.setHardness(0.5F);
        this.setLightLevel(0.7F);
    }

    public boolean isOpaqueCube() {
        return false;
    }
}
package GamrCorps.mystical.block;
import GamrCorps.mystical.functions.CoalTreeGenFunctions;
import GamrCorps.mystical.lib.Constants;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.world.World;

import java.util.Random;

public class BlockCoalSapling extends BlockBush {
    public BlockCoalSapling() {
        super(Material.leaves);
        this.setBlockName(Constants.nameBlockCoalSapling);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
        this.setBlockTextureName(Constants.MODID + ":coal_sapling");
    }

    public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entity, int l, float m, float n, float o) {

        if (entity.inventory.getCurrentItem() != null && entity.inventory.getCurrentItem().getItem() == Items.dye && entity.inventory.getCurrentItem().getItemDamage() == 15) {
            world.setBlock(i, j + 1, k, Blocks.coal_block, 0, 2);
            world.setBlock(i, j, k, Blocks.coal_block, 0, 2);
            world.setBlock(i, j + 1, k + 1, ModBlocks.coalLeaves, 0, 2);
            world.setBlock(i, j + 1, k - 1, ModBlocks.coalLeaves, 0, 2);
            world.setBlock(i + 1, j + 1, k, ModBlocks.coalLeaves, 0, 2);
            world.setBlock(i - 1, j + 1, k, ModBlocks.coalLeaves, 0, 2);
            world.setBlock(i, j + 2, k, ModBlocks.coalLeaves, 0, 2);
            if (entity instanceof EntityPlayer)
                ((EntityPlayer) entity).inventory.consumeInventoryItem(entity.inventory.getCurrentItem().getItem());
        }
        return true;
    }

    public int rand(Random xRandom) {
        return xRandom.nextInt(500) == 0 ? 1 : 0;

    }

    @Override
    public void updateTick(World world, int i, int j, int k, Random tRandom) {
        if (tRandom.nextInt(200) == 1) {
            CoalTreeGenFunctions.generateCoalTree(world, i, j, k);
        }

    }


}

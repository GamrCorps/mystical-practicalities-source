package GamrCorps.mystical.block;

import GamrCorps.mystical.gui.GUIS;
import GamrCorps.mystical.item.ModItems;
import GamrCorps.mystical.main.MysticalMod;
import GamrCorps.mystical.tileentity.TileEntityManaFurnaceGeneration;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockManaFurnaceGeneration extends BlockContainer{
    BlockManaFurnaceGeneration(){
        super(Material.iron);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta){
        return new TileEntityManaFurnaceGeneration();
    }

    @Override
    public boolean hasTileEntity(int metadata) {

        return true;
    }

    public boolean onBlockActivated(World world, int intx, int inty, int intz, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        TileEntityManaFurnaceGeneration temp = (TileEntityManaFurnaceGeneration) world.getTileEntity(intx, inty, intz);

        if (world.isRemote) {
            return true;
        } else if (player.inventory.getCurrentItem() != null && player.inventory.getCurrentItem().getItem() != null && player.inventory.getCurrentItem().getItem() == ModItems.itemMysticalGlass) {
            if (temp.getOwner() == null || temp.getOwner() == "") {
                temp.setOwner(player);
            }
            //TODO: temp.sendChatInfoToPlayer(player);
            return true;
        } else {
//            TileEntityDispenser tileentitydispenser = (TileEntityDispenser) world.getTileEntity(intx, inty, intz);

            if (temp != null) {
                player.openGui(MysticalMod.instance, GUIS.FURNACEGEN.ordinal(), world, intx, inty, intz);
            }

            return true;
        }
    }
}

package GamrCorps.mystical.block;

import GamrCorps.mystical.functions.TreeGenFunctions;
import GamrCorps.mystical.lib.Constants;
import net.minecraft.block.BlockBush;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.world.World;

public class BlockMutatedSapling extends BlockBush {
    public BlockMutatedSapling() {
        super(Material.leaves);
        this.setBlockName(Constants.nameMutatedSapling);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setBlockBounds(0F, 0F, 0F, 1F, 1F, 1F);
        this.setBlockTextureName(Constants.MODID + ":mutated_sapling-1");
    }

    @Override
    public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entity, int l, float m, float n, float o) {

        if (entity.inventory.getCurrentItem() != null && entity.inventory.getCurrentItem().getItem() == Items.dye && entity.inventory.getCurrentItem().getItemDamage() == 15) {
            TreeGenFunctions.generateTree("oak", world, ModBlocks.mutatedLog, ModBlocks.mutatedLeaves, i, j, k);
            if (entity instanceof EntityPlayer)
                ((EntityPlayer) entity).inventory.consumeInventoryItem(entity.inventory.getCurrentItem().getItem());
        }

        return true;
    }
}

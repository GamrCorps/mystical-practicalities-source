package GamrCorps.mystical.block;

import GamrCorps.mystical.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockMutatedStone extends Block {

    public BlockMutatedStone() {
        super(Material.rock);
        this.setBlockName(Constants.nameMutatedStone);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setBlockTextureName(Constants.MODID + ":mutated_stone");
        this.setResistance(7.0F);
        this.setHardness(5F);


    }
}
package GamrCorps.mystical.block;

import GamrCorps.mystical.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockMutatedLog extends Block {
    public BlockMutatedLog() {
        super(Material.wood);
        this.setBlockName(Constants.nameMutatedLog);
        this.setCreativeTab(CreativeTabs.tabBlock);
        this.setBlockTextureName(Constants.MODID + ":oak_log");
    }
}
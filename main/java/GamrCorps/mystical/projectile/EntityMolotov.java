package GamrCorps.mystical.projectile;


import GamrCorps.mystical.item.ModItems;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.init.Blocks;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;



public class EntityMolotov extends EntityThrowable {
    public EntityMolotov(World world) {
        super(world);
    }

    public EntityMolotov(World world, EntityLivingBase xBase) {
        super(world, xBase);
    }

    public EntityMolotov(World world, double x, double y, double z) {
        super(world, x, y, z);
    }

    protected void onImpact(MovingObjectPosition xPosition)
    {
        if (xPosition.entityHit != null)
        {
            byte b0 = 3;

            xPosition.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), (float) b0);
            this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, (float) (5.0D), true);
            EntityLivingBase pUse = this.getThrower();
            if (pUse instanceof EntityPlayer)
            {
                EntityPlayer entPlay = (EntityPlayer) pUse;
                if (!entPlay.capabilities.isCreativeMode)
                {
                    entPlay.inventory.consumeInventoryItem(ModItems.itemMolotov);
                }
                for (int i = -5; i <= 5; i++)
                {
                    for (int j = -5; j <= 5; j++)
                    {
                        for (int k = -5; k <= 5; j++)
                        {
                            if (this.worldObj.getBlock(i, j, k) != null && this.worldObj.getBlock(i, j, k) == Blocks.air && this.worldObj.getBlock(i, j - 1, k) != null && this.worldObj.getBlock(i, j - 1, k) != Blocks.air)
                            {
                                this.worldObj.setBlock(i, j, k, Blocks.fire);
                            }
                        }
                    }
                }
            }
            this.setDead();
        }

        /*
        for (int i = 0; i < 8; ++i)
        {
            this.worldObj.createExplosion(this, this.posX, this.posY, this.posZ, (float)(1.1D + this.rand.nextDouble() * .5D), true);
            this.setDead();
        }
        */

        if (!this.worldObj.isRemote) {
            this.setDead();
        }
    }
}

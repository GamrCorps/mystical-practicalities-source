package GamrCorps.mystical.lib;


import net.minecraft.item.Item;

public class Constants {
    //Mod Stuff
    public static final String MODID = "MysticalMod";
    public static final String VERSION = "1.7.10-0.1(Alpha)";
    public static final String MODNAME = "MysticalMod";
    //Items
    public static final String nameMolotov = "Molotov";
    public static final String nameMysticalGlass = "MysticalGlass";
    public static final String nameMutatedBlob = "MutatedBlob";
    public static final String nameUpgradePotentia = "UpgradePotentia";
    public static final String nameShuriken = "Shuriken";
    //Blocks
    public static final String nameMutatedSapling = "MutatedSapling";
    public static final String nameMutatedStone = "MutatedStone";
    public static final String nameMutatedLog = "MutatedLog";
    public static final String nameMutatedLeaves = "MutatedLeaves";
    public static final String nameBlockCoalSapling = "BlockCoalSapling";
    public static final String nameBlockCoalLeaves = "BlockCoalLeaves";
    public static final String nameQuarry = "MysticalQuarry";
    public static final String nameUpgradeContainer = "UpgradeContainer";
    public static final String nameFurnaceGen = "FurnaceGenerator";
    //Tile Entities
    public static final String nameTileQuarry = "TileQuarry";
    public static final String nameTileContainer = "TileUpgradeContainer";
    public static final String nameTileFurnaceGen = "TileFurnaceGen";

}

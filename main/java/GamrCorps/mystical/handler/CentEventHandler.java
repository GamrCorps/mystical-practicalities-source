package GamrCorps.mystical.handler;

import GamrCorps.mystical.block.ModBlocks;
import GamrCorps.mystical.functions.MiscFunctions;
import GamrCorps.mystical.item.ModItems;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent.Action;

public class CentEventHandler {

    public CentEventHandler() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public void playerInteract(PlayerInteractEvent event) {
        Action eventAction = event.action;
        World currentWorld = event.entityPlayer.worldObj;
        Block blockAffected = currentWorld.getBlock(event.x, event.y, event.z);
        ItemStack currentTool = event.entityPlayer.getCurrentEquippedItem();
        if (eventAction.RIGHT_CLICK_BLOCK != null) {
            if (currentWorld.getBlock(event.x, event.y, event.z) == Blocks.sapling && event.entityPlayer.inventory.getCurrentItem().getItem() == ModItems.itemMutatedBlob) {
                if (MiscFunctions.randNumInRange(0, 3) == 3) {
                    currentWorld.setBlock(event.x, event.y, event.z, ModBlocks.mutatedSapling, 0, 2);
                }
            }
            if (currentWorld.getBlock(event.x, event.y, event.z) == Blocks.stone && event.entityPlayer.inventory.getCurrentItem().getItem() == ModItems.itemMutatedBlob) {
                if (MiscFunctions.randNumInRange(0, 3) == 3) {
                    currentWorld.setBlock(event.x, event.y, event.z, ModBlocks.mutatedStone, 0, 2);
                }
            }
        }
    }

    @SubscribeEvent
    public void playerLogin(PlayerEvent.PlayerLoggedInEvent event) {
    }
}

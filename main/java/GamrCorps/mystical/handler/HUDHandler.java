package GamrCorps.mystical.handler;

import GamrCorps.mystical.block.ModBlocks;
import GamrCorps.mystical.recipes.RecipeHandler;
import GamrCorps.mystical.recipes.RecipeUpgradeCreation;
import GamrCorps.mystical.tileentity.TileEntityUpgradeContainer;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.ItemStack;
import net.minecraft.profiler.Profiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.lwjgl.opengl.GL11;

public class HUDHandler {
    @SubscribeEvent
    public void onDrawScreen(RenderGameOverlayEvent.Post event) {
        Minecraft mc = Minecraft.getMinecraft();
        Profiler profiler = mc.mcProfiler;
        if (event.type == RenderGameOverlayEvent.ElementType.ALL) {
            profiler.startSection("mystical-hud");
            MovingObjectPosition pos = mc.objectMouseOver;
            if (pos != null) {
                Block block = mc.theWorld.getBlock(pos.blockX, pos.blockY, pos.blockZ);
                if (block.hasTileEntity(0)) {
                    TileEntity tile = mc.theWorld.getTileEntity(pos.blockX, pos.blockY, pos.blockZ);
                    if (mc.theWorld.getBlock(tile.xCoord, tile.yCoord, tile.zCoord) != null && mc.theWorld.getBlock(tile.xCoord, tile.yCoord, tile.zCoord) == ModBlocks.upgradeContainer) {
                        TileEntityUpgradeContainer tileUpgrade = (TileEntityUpgradeContainer) mc.theWorld.getTileEntity(pos.blockX, pos.blockY, pos.blockZ);
                        ItemStack stack = mc.thePlayer.getHeldItem();
                        ItemStack stack1 = mc.thePlayer.getItemInUse();
                        ItemStack stack2 = mc.thePlayer.getCurrentEquippedItem();
                        if (stack != null) {
                            if (tile != null) {
                                renderPoolRecipeHUD(event.resolution, stack, stack1, stack2);
                                //renderGlassStats(event.resolution,stack,stack1,stack2,mc.thePlayer);
                            }
                        }
                    }
                }
            }
            profiler.endSection();
        }
    }

    private void renderPoolRecipeHUD(ScaledResolution res, ItemStack stack, ItemStack stack1, ItemStack stack2) {
        Minecraft mc = Minecraft.getMinecraft();
        Profiler profiler = mc.mcProfiler;
        profiler.startSection("poolRecipe");
        FontRenderer font = mc.fontRenderer;
        for (RecipeUpgradeCreation recipe : RecipeHandler.upgradeCreationRecipes) {
            if (recipe.matches(stack) || recipe.matches(stack1) || recipe.matches(stack2)) {
                int x = res.getScaledWidth() / 2 - 11;
                int y = res.getScaledHeight() / 2 + 10;
                int v = mc.thePlayer.getCommandSenderName().equals("haighyorkie") && mc.thePlayer.isSneaking() ? 23 : 8;
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                GL11.glColor4f(1F, 1F, 1F, 1F);
                int sx = res.getScaledWidth() / 2 - 17;
                int sy = res.getScaledHeight() / 2 + 2;
                int dx = (res.getScaledWidth() / 4) + (res.getScaledWidth() / 2);
                int dy = (res.getScaledHeight() / 4) + (res.getScaledHeight() / 2);
                net.minecraft.client.renderer.RenderHelper.enableGUIStandardItemLighting();
                RenderItem.getInstance().renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, recipe.getOutput(), dx - 8, dy - 8);
                RenderItem.getInstance().renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, recipe.getOutput(), dx - 8, dy - 8);
                font.drawStringWithShadow(recipe.getOutput().getDisplayName() + " (" + recipe.getAmount() + ")", dx + 10, dy, 0xFFFFFFFF);
                font.drawStringWithShadow("Upgrade Name: " + recipe.getName(), dx, dy + 10, 0xFFFFFFFF);
                font.drawStringWithShadow("Upgrade Type: " + recipe.getType(), dx, dy + 20, 0xFFFFFFFF);
                font.drawStringWithShadow("Upgrade Modifier: " + recipe.getModifier(), dx, dy + 30, 0xFFFFFFFF);
                font.drawStringWithShadow("Recipe: ", dx - 48, dy, 0xFFFFFFFF);
                //RenderItem.getInstance().renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, stack, x + 20, y);
                //RenderItem.getInstance().renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, stack, x + 20, y);
                //font.drawStringWithShadow(stack.getDisplayName() + " (10)", sx + 10, sy + 8, 0xFFFFFFFF);
                net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                GL11.glDisable(GL11.GL_LIGHTING);
                GL11.glDisable(GL11.GL_BLEND);
                break;
            }
        }
        profiler.endSection();
    }

    /*private void renderGlassStats(ScaledResolution res, ItemStack stack, ItemStack stack1, ItemStack stack2, EntityPlayer player) {
        Minecraft mc = Minecraft.getMinecraft();
        Profiler profiler = mc.mcProfiler;
        profiler.startSection("poolRecipe");
        FontRenderer font = mc.fontRenderer;
            if (stack.getItem() == ModItems.itemMysticalGlass || stack1.getItem() == ModItems.itemMysticalGlass || stack2.getItem() == ModItems.itemMysticalGlass) {
                World worldSave = MinecraftServer.getServer().worldServers[0];
                ManaNetwork data = (ManaNetwork) worldSave.loadItemData(ManaNetwork.class, player.getCommandSenderName());
                int x = res.getScaledWidth() / 2 - 11;
                int y = res.getScaledHeight() / 2 + 10;
                int v = mc.thePlayer.getCommandSenderName().equals("haighyorkie") && mc.thePlayer.isSneaking() ? 23 : 8;
                GL11.glEnable(GL11.GL_BLEND);
                GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                GL11.glColor4f(1F, 1F, 1F, 1F);
                int sx = res.getScaledWidth() / 2 - 17;
                int sy = res.getScaledHeight() / 2 + 2;
                int dx = (res.getScaledWidth() / 4) + (res.getScaledWidth() / 2);
                int dy = (res.getScaledHeight() / 4) + (res.getScaledHeight() / 2);
                net.minecraft.client.renderer.RenderHelper.enableGUIStandardItemLighting();
                RenderItem.getInstance().renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, stack, dx - 8, dy - 8);
                RenderItem.getInstance().renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, stack, dx - 8, dy - 8);
                //font.drawStringWithShadow(recipe.getOutput().getDisplayName() + " (" + recipe.getAmount() + ")", dx + 10, dy, 0xFFFFFFFF);
                font.drawStringWithShadow("Current Mana: " + data.currentMana, dx, dy + 10, 0xFFFFFFFF);
                //font.drawStringWithShadow("Mana: ", dx - 48, dy, 0xFFFFFFFF);
                //RenderItem.getInstance().renderItemAndEffectIntoGUI(mc.fontRenderer, mc.renderEngine, stack, x + 20, y);
                //RenderItem.getInstance().renderItemOverlayIntoGUI(mc.fontRenderer, mc.renderEngine, stack, x + 20, y);
                //font.drawStringWithShadow(stack.getDisplayName() + " (10)", sx + 10, sy + 8, 0xFFFFFFFF);
                net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                GL11.glDisable(GL11.GL_LIGHTING);
                GL11.glDisable(GL11.GL_BLEND);


        }
        profiler.endSection();
    }*/
}

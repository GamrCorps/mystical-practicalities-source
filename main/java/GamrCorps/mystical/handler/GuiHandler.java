package GamrCorps.mystical.handler;

import GamrCorps.mystical.container.ContainerQuarry;
import GamrCorps.mystical.gui.GUIS;
import GamrCorps.mystical.gui.GuiFurnaceGen;
import GamrCorps.mystical.gui.GuiQuarry;
import GamrCorps.mystical.tileentity.TileEntityManaFurnaceGeneration;
import GamrCorps.mystical.tileentity.TileEntityQuarry;
import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Created by matthewmccaskill on 10/25/14.
 */
public class GuiHandler implements IGuiHandler {

    //returns an instance of the Container you made earlier
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world,
                                      int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(x, y, z);
        if (tileEntity instanceof TileEntityQuarry) {
            return new ContainerQuarry(player.inventory, (TileEntityQuarry) tileEntity);
        }
        return null;
    }

    //returns an instance of the Gui you made earlier
    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tileEntity = world.getTileEntity(x, y, z);
        if(id == GUIS.QUARRY.ordinal() && tileEntity instanceof TileEntityQuarry) return new GuiQuarry(player.inventory,(TileEntityQuarry) tileEntity);
        else if (id == GUIS.FURNACEGEN.ordinal() && tileEntity instanceof TileEntityManaFurnaceGeneration) return new GuiFurnaceGen(player.inventory, (TileEntityManaFurnaceGeneration) tileEntity);

        //TileEntity tileEntity = world.getTileEntity(x, y, z);
        //if (tileEntity instanceof TileEntityQuarry) {
        //    return new GuiQuarry(player.inventory, (TileEntityQuarry) tileEntity);
        //}
        return null;

    }
}

package GamrCorps.mystical.GUD;

import GamrCorps.mystical.lib.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class GUD {

    public static void checkVersion() {

        try {

            URL url = new URL("https://e25d230f919c5d6278e81c82d5e68a6a238a70a4-www.googledrive.com/host/0Byu4AFDjMDT1MVhLMGxMa0FnaHc");
            String currentVersion = Constants.VERSION;

            // read text returned by server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
                if (line != currentVersion) {
                    System.out.println("[GUD] Running outdated version of mod " + Constants.MODNAME);
                    System.out.println("[GUD] Current Version: " + line);
                } else {
                    System.out.println("[GUD] Running current version of mod " + Constants.MODNAME);
                }
            }
            in.close();

        } catch (MalformedURLException e) {
            System.out.println("[GUD] Malformed URL: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("[GUD] I/O Error: " + e.getMessage());
        }

    }

}
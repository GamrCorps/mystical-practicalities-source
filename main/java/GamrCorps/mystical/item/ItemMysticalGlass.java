package GamrCorps.mystical.item;

import GamrCorps.mystical.block.ModBlocks;
import GamrCorps.mystical.lib.Constants;
import GamrCorps.mystical.main.MysticalMod;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;

import java.util.List;

public class ItemMysticalGlass extends Item {
    public ItemMysticalGlass() {
        super();
        this.maxStackSize = 1;
        setCreativeTab(CreativeTabs.tabTools);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(Constants.MODID + ":magical_glass");
    }

    @Override
    public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
        par3List.add("See the hidden features");
        par3List.add("of blocks and items...");
        par3List.add("...at a small cost...");

        if (!(par1ItemStack.stackTagCompound == null)) {
            par3List.add("Owner: " + par1ItemStack.stackTagCompound.getString("playerOwnerName"));
        }
    }

    @SideOnly(Side.CLIENT)
    public MovingObjectPosition rayTrace(EntityPlayer player, double p_70614_1_, float p_70614_3_) {
        Vec3 vec3 = player.getPosition(p_70614_3_);
        Vec3 vec31 = player.getLook(p_70614_3_);
        Vec3 vec32 = vec3.addVector(vec31.xCoord * p_70614_1_, vec31.yCoord + player.eyeHeight * p_70614_1_, vec31.zCoord * p_70614_1_);
        return player.worldObj.func_147447_a(vec3, vec32, false, false, true);
    }

    @Override
    public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, World par3World, int par4, int par5, int par6, int par7, float par8, float par9, float par10) {
        if (par2EntityPlayer.isSneaking()) {
            Block block = par3World.getBlock(par4, par5, par6);

            if (block != null) {
                MysticalMod.log.log(Level.INFO, "[MysticalGlass] Block: " + block.getUnlocalizedName());
                if (block == ModBlocks.quarry) {
                    par2EntityPlayer.addChatComponentMessage(new ChatComponentText("You feel the presence of a miner in this block."));
                } else if (block == ModBlocks.upgradeContainer) {
                    par2EntityPlayer.addChatComponentMessage(new ChatComponentText("You feel the presence of a innovator in this block."));
                } else {
                    par2EntityPlayer.addChatComponentMessage(new ChatComponentText("You feel dullness radiating from this block."));
                }
            }
        }

        return false;
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
        ModEnergyItem.setCheckOwner(par1ItemStack, par3EntityPlayer);

        if (par3EntityPlayer.worldObj.isRemote) {
            return par1ItemStack;
        }

        NBTTagCompound itemTag = par1ItemStack.stackTagCompound;

        if (itemTag == null || itemTag.getString("playerOwnerName").equals("")) {
            return par1ItemStack;
        }

        final int mopx = this.rayTrace(par3EntityPlayer, 200, 1.0F).blockX;
        final int mopy = this.rayTrace(par3EntityPlayer, 200, 1.0F).blockY;
        final int mopz = this.rayTrace(par3EntityPlayer, 200, 1.0F).blockZ;
        final World world = par2World;
        /*
        Block mopBlock = BlockInfoProvider.instance.getBlock();
        //MovingObjectPosition mopBlockCoords = BlockInfoProvider.instance.getPosition();

        //IChatComponent iChat = null;

        //iChat.appendText(mopx + ","+ mopy + ","+ mopz + " " + world.getBlock(mopx, mopy, mopz));

        if (mopBlock != Blocks.air && mopBlock != null) {
            System.out.println(mopx + "," + mopy + "," + mopz + " " + par2World.getBlock(mopx, mopy, mopz));
            MysticalMod.log.log(Level.INFO, "Block at " + mopBlockCoords.blockX + ", " + mopBlockCoords.blockY + ", " + mopBlockCoords.blockZ + " is " + mopBlock.getUnlocalizedName());
            //par3EntityPlayer.addChatMessage(iChat);
        }
        */
        return par1ItemStack;
    }
}

package GamrCorps.mystical.item;

import GamrCorps.mystical.functions.PlayerFunctions;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

public class ModEnergyItem {
    public static void setCheckOwner(ItemStack item, EntityPlayer player) {
        if (item.stackTagCompound == null) {
            item.setTagCompound(new NBTTagCompound());
        }

        if (item.stackTagCompound.getString("playerOwnerName").equals("")) {
            item.stackTagCompound.setString("playerOwnerName", PlayerFunctions.getUsername(player));

            NBTTagCompound playerTag = player.getEntityData();
            NBTBase manaTag = playerTag.getTag("currentMana");
            int manaTagInt = playerTag.getInteger("currentMana");
            if (manaTag == null || manaTagInt == 0) {
                playerTag.setInteger("currentMana", 5);
            }
            playerTag.setInteger("currentMana", manaTagInt - 5);
        }
    }
}
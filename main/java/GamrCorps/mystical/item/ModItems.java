package GamrCorps.mystical.item;

import GamrCorps.mystical.lib.Constants;
import GamrCorps.mystical.main.MysticalMod;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

public class ModItems {
    public static Item itemMysticalGlass;
    public static Item itemMutatedBlob;
    public static Item itemUpgradePotentia;
    public static Item itemMolotov;
    public static Item itemShuriken;
    public static void init() {
        itemMysticalGlass = new ItemMysticalGlass().setUnlocalizedName(Constants.nameMysticalGlass).setCreativeTab(MysticalMod.mysticalCreativeTab);
        itemMutatedBlob = new ItemMutatedBlob().setUnlocalizedName(Constants.nameMutatedBlob).setCreativeTab(MysticalMod.mysticalCreativeTab);
        itemUpgradePotentia = new ItemUpgradePotentia().setUnlocalizedName(Constants.nameUpgradePotentia).setCreativeTab(MysticalMod.mysticalCreativeTab);
        itemMolotov = new ItemMolotov().setUnlocalizedName(Constants.nameMolotov).setCreativeTab(MysticalMod.mysticalCreativeTab);
        itemShuriken = new ItemShuriken().setUnlocalizedName(Constants.nameShuriken).setCreativeTab(MysticalMod.mysticalCreativeTab);
    }

    public static void registerItems() {
        GameRegistry.registerItem(ModItems.itemMysticalGlass, Constants.nameMysticalGlass);
        GameRegistry.registerItem(ModItems.itemMutatedBlob, Constants.nameMutatedBlob);
        GameRegistry.registerItem(ModItems.itemUpgradePotentia, Constants.nameUpgradePotentia);
        GameRegistry.registerItem(ModItems.itemMolotov, Constants.nameMolotov);
        GameRegistry.registerItem(ModItems.itemShuriken, Constants.nameShuriken);
    }

}

package GamrCorps.mystical.item;

import GamrCorps.mystical.lib.Constants;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemUpgradePotentia extends Item {
    public ItemUpgradePotentia() {
        super();
        this.maxStackSize = 64;
        this.setCreativeTab(CreativeTabs.tabMisc);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(Constants.MODID + ":upgrade_potentia");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
        //ManaNetworkHandler.setCurrentEssence(par3EntityPlayer.getCommandSenderName(),ManaNetworkHandler.getCurrentEssence(par3EntityPlayer.getCommandSenderName())+100);
        //par3EntityPlayer.addChatComponentMessage(new ChatComponentText("Mana: "+ ManaNetworkHandler.getCurrentEssence(par3EntityPlayer.getCommandSenderName())));
        return par1ItemStack;
    }

}
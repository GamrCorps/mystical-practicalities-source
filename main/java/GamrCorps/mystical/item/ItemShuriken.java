package GamrCorps.mystical.item;


import GamrCorps.mystical.entity.projectile.EntityShuriken;
import GamrCorps.mystical.lib.Constants;
import GamrCorps.mystical.main.MysticalMod;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.lwjgl.opengl.GL11;

import javax.swing.plaf.basic.BasicComboBoxUI;

public class ItemShuriken extends Item

{
    public ItemShuriken() {
        super();
        this.maxStackSize = 8;
        this.setCreativeTab(MysticalMod.mysticalCreativeTab);
    }


    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player) {
        if (!player.capabilities.isCreativeMode) {
            --itemstack.stackSize;
        }

        world.playSoundAtEntity(player, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!world.isRemote) {
            world.spawnEntityInWorld(new EntityShuriken(world, player));
        }

        return itemstack;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIcon = iconRegister.registerIcon(Constants.MODID + ":shurikenl");
        this.setTextureName(Constants.MODID + ":shurikenl");
    }
}

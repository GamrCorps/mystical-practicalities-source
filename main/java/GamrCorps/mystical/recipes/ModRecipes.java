package GamrCorps.mystical.recipes;

import GamrCorps.mystical.item.ModItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ModRecipes {
    public static void registerCraftingRecipes() {
        //Declare Item Stacks
        ItemStack stone = new ItemStack(Item.getItemFromBlock(Blocks.stone));
        ItemStack stick = new ItemStack(Items.stick);
        ItemStack bonemeal = new ItemStack(Items.dye);
        bonemeal.setItemDamage(15);
        ItemStack ferspieye = new ItemStack(Items.fermented_spider_eye);
        ItemStack netherwart = new ItemStack(Items.nether_wart);

        //Register Recipes
        CraftingRecipeHandler.addShapedCraftingRecipe(new ItemStack(Items.stone_pickaxe), 3, stone, stone, stone, null, stick, null, null, stick, null);
        CraftingRecipeHandler.addShapedCraftingRecipe(new ItemStack(ModItems.itemMutatedBlob), 3, ferspieye, netherwart, ferspieye, netherwart, bonemeal, netherwart, ferspieye, netherwart, ferspieye);
    }
}

package GamrCorps.mystical.recipes;

//Created by santa on 11/11/1111.

import GamrCorps.mystical.block.ModBlocks;
import GamrCorps.mystical.item.ModItems;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

//Recipes

public class RecipeHandler {
    public static List<RecipeUpgradeCreation> upgradeCreationRecipes = new ArrayList<RecipeUpgradeCreation>();

    public static void registerItems() {
        GameRegistry.addRecipe(new ItemStack(ModItems.itemMysticalGlass), new Object[]{"GXG", "GGG", "GXG", 'X', Item.getItemFromBlock(Blocks.dirt), 'G', Item.getItemFromBlock(Blocks.glass)});
        //GameRegistry.addShapelessRecipe(new ItemStack(ModItems.itemMutatedBlob),Item.itemRegistry.getObject("stick"));
        //GameRegistry.addShapelessRecipe(new ItemStack(ModItems.itemMutatedBlob), Item.getItemFromBlock(ModBlocks.mutatedStone), Item.getItemFromBlock(Blocks.dirt));
        ModUpgradeRecipes.init();
        //GameRegistry.addRecipe(new ItemStack(Item.getItemFromBlock(ModBlocks.coalSapling)), new Object[]{"GXG", "GGG", "GXG", 'X', Item.getItemFromBlock(Blocks.dirt), 'G', Item.getItemFromBlock(Blocks.glass)});

    }

    public static RecipeUpgradeCreation registerUpgradeCreationRecipe(ItemStack output, int amount, String name, String type, int modifier) {
        RecipeUpgradeCreation recipe = new RecipeUpgradeCreation(output, amount, name, type, modifier);
        upgradeCreationRecipes.add(recipe);
        return recipe;
    }
}

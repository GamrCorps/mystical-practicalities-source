package GamrCorps.mystical.recipes;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class CraftingRecipeHandler {
    public static void addShapedCraftingRecipe(ItemStack output, int grid, ItemStack topleft, ItemStack topmid, ItemStack topright, ItemStack midleft, ItemStack mid, ItemStack midright, ItemStack botleft, ItemStack botmid, ItemStack botright) {
        String a = "", b = "", c = "", d = "", e = "", f = "", g = "", h = "", i = "";
        Object[] recipeObject = {};
        Object[] recipeObjectTemp = {};
        if (grid == 3) {
            if (topleft != null) {
                a = "A";
                recipeObject = appendValue(recipeObject, 'A');
                recipeObject = appendValue(recipeObject, topleft);
            } else {
                a = " ";
            }
            if (topmid != null) {
                b = "B";
                recipeObject = appendValue(recipeObject, 'B');
                recipeObject = appendValue(recipeObject, topmid);
            } else {
                b = " ";
            }
            if (topright != null) {
                c = "C";
                recipeObject = appendValue(recipeObject, 'C');
                recipeObject = appendValue(recipeObject, topright);
            } else {
                c = " ";
            }
            if (midleft != null) {
                d = "D";
                recipeObject = appendValue(recipeObject, 'D');
                recipeObject = appendValue(recipeObject, midleft);
            } else {
                d = " ";
            }
            if (mid != null) {
                e = "E";
                recipeObject = appendValue(recipeObject, 'E');
                recipeObject = appendValue(recipeObject, mid);
            } else {
                e = " ";
            }
            if (midright != null) {
                f = "F";
                recipeObject = appendValue(recipeObject, 'F');
                recipeObject = appendValue(recipeObject, midright);
            } else {
                f = " ";
            }
            if (botleft != null) {
                g = "G";
                recipeObject = appendValue(recipeObject, 'G');
                recipeObject = appendValue(recipeObject, botleft);
            } else {
                g = " ";
            }
            if (botmid != null) {
                h = "H";
                recipeObject = appendValue(recipeObject, 'H');
                recipeObject = appendValue(recipeObject, botmid);
            } else {
                h = " ";
            }
            if (botright != null) {
                i = "I";
                recipeObject = appendValue(recipeObject, 'I');
                recipeObject = appendValue(recipeObject, botright);
            } else {
                i = " ";
            }
            recipeObjectTemp = appendValue(recipeObjectTemp, a + b + c);
            recipeObjectTemp = appendValue(recipeObjectTemp, d + e + f);
            recipeObjectTemp = appendValue(recipeObjectTemp, g + h + i);
            for (Object temp : recipeObject) {
                recipeObjectTemp = appendValue(recipeObjectTemp, temp);
            }
        }
        GameRegistry.addShapedRecipe(output, recipeObjectTemp);
    }

    public static Object[] appendValue(Object[] obj, Object newObj) {

        ArrayList<Object> temp = new ArrayList<Object>(Arrays.asList(obj));
        temp.add(newObj);
        return temp.toArray();

    }
}

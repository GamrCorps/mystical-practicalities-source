package GamrCorps.mystical.recipes;

import net.minecraft.item.ItemStack;

public class RecipeUpgradeCreation {
    ItemStack output;
    int amount;
    String name;
    String type;
    int modifier;

    public RecipeUpgradeCreation(ItemStack output, int amount, String name, String type, int modifier) {
        this.output = output;
        this.amount = amount;
        this.name = name;
        this.type = type;
        this.modifier = modifier;
    }

    public boolean matches(ItemStack stack) {
        if (output != null && stack != null) {
            return stack.getItem() == output.getItem();
        }
        return false;
    }

    public ItemStack getOutput() {
        return output;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getModifier() {
        return modifier;
    }

}
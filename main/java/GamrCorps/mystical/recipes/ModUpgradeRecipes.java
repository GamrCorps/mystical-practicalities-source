package GamrCorps.mystical.recipes;

import GamrCorps.mystical.item.ModItems;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class ModUpgradeRecipes {
    public static RecipeUpgradeCreation potentiaRecipe;
    public static RecipeUpgradeCreation enderEyeRangeUpgrade;
    public static RecipeUpgradeCreation sugarRecipe;

    public static void init() {
        potentiaRecipe = RecipeHandler.registerUpgradeCreationRecipe(new ItemStack(ModItems.itemUpgradePotentia), 10, "Potentia", "Speed", 2);
        enderEyeRangeUpgrade = RecipeHandler.registerUpgradeCreationRecipe(new ItemStack(Items.ender_eye), 10, "Ender Range", "Range", 10);
        sugarRecipe = RecipeHandler.registerUpgradeCreationRecipe(new ItemStack(Items.sugar), 1, "Sugar High", "Speed", 3);
    }
}

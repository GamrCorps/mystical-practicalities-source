package GamrCorps.mystical.creativetab;

import GamrCorps.mystical.item.ModItems;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class MysticalCreativeTab extends CreativeTabs {

    public MysticalCreativeTab(int id, String unlocalizedName) {

        super(id, unlocalizedName);
        this.setBackgroundImageName("mysticalTab.png");
    }

    @SideOnly(Side.CLIENT)
    public Item getTabIconItem() {

        //return Item.getItemFromBlock(Blocks.chest);
        return ModItems.itemMysticalGlass;
    }

    public boolean hasSearchBar() {
        return true;
    }
}
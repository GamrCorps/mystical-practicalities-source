package GamrCorps.mystical.proxy;

import GamrCorps.mystical.entity.projectile.EntityMolotov;
import GamrCorps.mystical.entity.projectile.RenderMolotov;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.world.World;

public class ClientProxy extends CommonProxy {
    @Override
    public World getClientWorld() {return FMLClientHandler.instance().getClient().theWorld;}
//    public void RegisterRenderThings(){
//        RenderingRegistry.registerEntityRenderingHandler(EntityMolotov.class, new RenderMolotov());
//    }
}


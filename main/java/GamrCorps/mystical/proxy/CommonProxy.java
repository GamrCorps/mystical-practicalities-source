package GamrCorps.mystical.proxy;

import GamrCorps.mystical.lib.Constants;
import GamrCorps.mystical.tileentity.TileEntityManaFurnaceGeneration;
import GamrCorps.mystical.tileentity.TileEntityQuarry;
import GamrCorps.mystical.tileentity.TileEntityUpgradeContainer;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class CommonProxy {

    // Client stuff
    public void registerRenderers() {
        // Nothing here as the server doesn't render graphics!
    }

    public void registerEntities() {
    }

    public World getClientWorld() {
        return null;
    }

    public void registerActions() {
    }

    public void registerEvents() {

    }

    public void registerSoundHandler() {
        // Nothing here as this is a server side proxy
    }

    public void registerTileEntities() {
        /*
        GameRegistry.registerTileEntity(TileEntityQuarry.class, Constants.nameTileQuarry);
        GameRegistry.registerTileEntity(TileEntityUpgradeContainer.class, Constants.nameTileContainer);
        */
        registerTile(TileEntityQuarry.class, Constants.nameTileQuarry);
        registerTile(TileEntityUpgradeContainer.class, Constants.nameTileContainer);
        registerTile(TileEntityManaFurnaceGeneration.class, Constants.nameTileFurnaceGen);
    }

    public void registerEntityTrackers() {

    }

    public void registerTickHandlers() {
    }

    public void InitRendering() {
    }

    private static void registerTile(Class<? extends TileEntity> clazz, String key) {
        GameRegistry.registerTileEntityWithAlternatives(clazz, Constants.MODID + key, key);
    }
}

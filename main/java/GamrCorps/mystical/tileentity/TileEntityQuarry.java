package GamrCorps.mystical.tileentity;

import GamrCorps.mystical.block.BlockQuarry;
import GamrCorps.mystical.block.ModBlocks;
import GamrCorps.mystical.handler.ManaNetworkHandler;
import GamrCorps.mystical.recipes.RecipeHandler;
import GamrCorps.mystical.recipes.RecipeUpgradeCreation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import java.util.Random;

public class TileEntityQuarry extends TileEntity implements IInventory {

    //TODO: FIX LIT REDSTONE ORE
    //TODO: FIX NETHER QUARTZ ORE
    //TODO: FIX STILL DESTROYS AFTER STACK LIMIT IS REACHED
    //TODO: FIX QUARRY Dispenser BLOCK NAME
    private ItemStack[] quarryContents = new ItemStack[9];
    private Random field_146021_j = new Random();
    protected String field_146020_a;
    int cooldown = 0;
    int delay = 120;
    int maxDelay = 120;
    int range = 4;
    int rangeY = 32;
    int maxRange = 4;
    int maxRangeY = 32;
    int currentAmount = 0;
    int minedAmount = 0;
    int numPlayersUsing = 0;
    String owner = "";

    World worldObj2 = this.worldObj;

    public static final String publicName = "tileEntityQuarry";
    private String name = "tileEntityQuarry";

    public String getName() {

        return name;
    }

    @Override
    public String getInventoryName() {
        return "Mystical Quarry";
    }

    @Override
    public void updateEntity() {
        TileEntityUpgradeContainer upgradeContainer = null;
        if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer) {
            upgradeContainer = (TileEntityUpgradeContainer) this.worldObj.getTileEntity(this.xCoord, this.yCoord + 1, this.zCoord);
        }
        if (!worldObj.isRemote) {
            checkForUpgrades();
            if (pollOres() > 0 && this.worldObj != null && this.owner != "" && ManaNetworkHandler.getCurrentEssence(this.owner) != 0 && (ManaNetworkHandler.getCurrentEssence(this.owner) - (this.maxDelay / this.delay)) >= 0) {

                /*if (worldObj.getBlock(this.xCoord,this.yCoord+1,this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null && upgradeContainer.currentUpgrade == "Potentia") {
                    delay = maxDelay/2;
                } else if (worldObj.getBlock(this.xCoord,this.yCoord+1,this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null && upgradeContainer.currentUpgrade != "Potentia") {
                    delay = maxDelay;
                }*/
                cooldown++;
                if (cooldown >= delay) {
                    //System.out.println(cooldown + ":" + delay + ":" + range + ":" + rangeY);
                    search:
                    for (int i = 0 - range; i <= range; i++) {
                        if (worldObj2 == null) {
                            System.out.println("WorldObj is null!");
                        }
                        for (int j = 0 - rangeY; j < 0; j++) {
                            for (int k = 0 - range; k <= range; k++) {
                                //System.out.println("Searching Relative At " + i + "," + j + "," + k);
                                if (this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k).getUnlocalizedName() != null) {
                                    if (this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k).getUnlocalizedName().toLowerCase().contains("ore") || this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k) == Blocks.quartz_ore) {
                                        //System.out.println("Found " + this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k) + " at " + (this.xCoord + i) + "," + (this.yCoord + j) + "," + (this.zCoord + k));
                                        //EntityItem var14 = new EntityItem(this.getWorldObj(),(double)(this.xCoord),(double)(this.yCoord+1),(double)(this.zCoord),new ItemStack(this.getWorldObj().getBlock(this.xCoord+i, this.yCoord+j, this.zCoord+k), 1, 0));
                                        if (this.getStackInSlot(0) == null || this.getStackInSlot(1) == null || this.getStackInSlot(2) == null || this.getStackInSlot(3) == null || this.getStackInSlot(4) == null || this.getStackInSlot(5) == null || this.getStackInSlot(6) == null || this.getStackInSlot(7) == null || this.getStackInSlot(8) == null) {
                                            //var14.delayBeforeCanPickup = 1;
                                            //this.getWorldObj().spawnEntityInWorld(var14);
                                            for (int m = 0; m < this.getSizeInventory(); m++) {
                                                if (this.getStackInSlot(m) != null && this.getStackInSlot(m).getItem() == Item.getItemFromBlock(this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k)) && this.getStackInSlot(m).stackSize <= 63) {
                                                    ItemStack temp = this.getStackInSlot(m);
                                                    temp.stackSize++;
                                                    this.setInventorySlotContents(m, temp);
                                                    this.getWorldObj().setBlockToAir(this.xCoord + i, this.yCoord + j, this.zCoord + k);
                                                    this.minedAmount++;
                                                    if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer) {
                                                        upgradeContainer.decAmount(1);
                                                        ManaNetworkHandler.syphonFromNetwork(this.owner, 1);
                                                    }
                                                    break search;
                                                } else {
                                                    continue;
                                                }
                                            }

                                            for (int n = 0; n < this.getSizeInventory(); n++) {
                                                if (this.getStackInSlot(n) == null) {
                                                    this.setInventorySlotContents(n, new ItemStack(this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k), 1, 0));
                                                    this.getWorldObj().setBlockToAir(this.xCoord + i, this.yCoord + j, this.zCoord + k);
                                                    this.minedAmount++;
                                                    if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null) {
                                                        upgradeContainer.decAmount(1);
                                                        ManaNetworkHandler.syphonFromNetwork(this.owner, 1);
                                                    }
                                                    break search;
                                                }
                                            }
                                        } else {
                                            for (int o = 0; o < this.getSizeInventory(); o++) {
                                                if (this.getStackInSlot(o).getItem() == Item.getItemFromBlock(this.worldObj.getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k))) {
                                                    //var14.delayBeforeCanPickup = 1;
                                                    //this.getWorldObj().spawnEntityInWorld(var14);
                                                    for (int m = 0; m < this.getSizeInventory(); m++) {
                                                        if (this.getStackInSlot(m).getItem() == Item.getItemFromBlock(this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k)) && this.getStackInSlot(m).stackSize < 64) {
                                                            ItemStack temp = this.getStackInSlot(m);
                                                            temp.stackSize++;
                                                            this.setInventorySlotContents(m, temp);
                                                            this.getWorldObj().setBlockToAir(this.xCoord + i, this.yCoord + j, this.zCoord + k);
                                                            this.minedAmount++;
                                                            if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null) {
                                                                upgradeContainer.decAmount(1);
                                                                ManaNetworkHandler.syphonFromNetwork(this.owner, 1);
                                                            }
                                                            break search;
                                                        }
                                                    }

                                                    for (int n = 0; n < this.getSizeInventory(); n++) {
                                                        if (this.getStackInSlot(n) == null) {
                                                            this.setInventorySlotContents(n, new ItemStack(this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k), 1, 0));
                                                            this.getWorldObj().setBlockToAir(this.xCoord + i, this.yCoord + j, this.zCoord + k);
                                                            this.minedAmount++;
                                                            if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null) {
                                                                upgradeContainer.decAmount(1);
                                                                ManaNetworkHandler.syphonFromNetwork(this.owner, 1);
                                                            }
                                                            break search;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        this.getWorldObj().setBlockToAir(this.xCoord + i, this.yCoord + j, this.zCoord + k);
                                        break search;
                                    }
                                }
                            }
                            cooldown = 0;
                        }
                    }
                }
            }
        }
    }

    public int pollOres() {
        currentAmount = 0;
        for (int i = 0 - range; i <= range; i++) {
            for (int j = 0 - rangeY; j < 0; j++) {
                for (int k = 0 - range; k <= range; k++) {
                    if (this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k).getUnlocalizedName() != null && this.getWorldObj().getBlock(this.xCoord + i, this.yCoord + j, this.zCoord + k).getUnlocalizedName().toLowerCase().contains("ore")) {
                        currentAmount++;
                    }
                }
            }
        }
        return currentAmount;
    }

    public void sendChatInfoToPlayer(EntityPlayer player) {
        player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Owner(s): ["+this.owner+"]"));
        player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Speed: 1 Block per " + this.delay / 20 + " seconds"));
        player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Range: +/-" + this.range + ",-" + this.rangeY + ",+/-" + this.range));
        player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Current Cooldown: " + this.cooldown));
        player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Mined Blocks: " + this.minedAmount));
        player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Blocks Left: " + this.pollOres()));
        if (this.worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer) {
            TileEntityUpgradeContainer upgradeContainer = (TileEntityUpgradeContainer) this.worldObj.getTileEntity(this.xCoord, this.yCoord + 1, this.zCoord);
            player.addChatComponentMessage(new ChatComponentText("[Mystical Quarry] Current Upgrade: " + upgradeContainer.getUpgrade() + "x" + upgradeContainer.getAmount()));
        }
    }

    public void setOwner(EntityPlayer player){
        this.owner = player.getCommandSenderName();
    }
    /*
    int cooldown = 0;
    int delay = 120;
    int maxDelay = 120;
    int range = 4;
    int rangeY = 32;
    int currentAmount = 0;
    int minedAmount = 0;
     */
    @Override
    public void readFromNBT(NBTTagCompound p_145839_1_) {
        super.readFromNBT(p_145839_1_);
        NBTTagList nbttaglist = p_145839_1_.getTagList("Items", 10);
        this.quarryContents = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i) {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 0 && j < this.quarryContents.length) {
                this.quarryContents[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }

        if (p_145839_1_.hasKey("CustomName", 8)) {
            this.field_146020_a = p_145839_1_.getString("CustomName");
        }
        readCustomNBT(p_145839_1_);
    }

    @Override
    public void writeToNBT(NBTTagCompound p_145841_1_) {
        super.writeToNBT(p_145841_1_);
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.quarryContents.length; ++i) {
            if (this.quarryContents[i] != null) {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte) i);
                this.quarryContents[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        p_145841_1_.setTag("Items", nbttaglist);

        if (this.hasCustomInventoryName()) {
            p_145841_1_.setString("CustomName", this.field_146020_a);
        }
        writeCustomNBT(p_145841_1_);
    }

    public void readCustomNBT(NBTTagCompound nbt) {
        this.cooldown = nbt.getInteger("Cooldown");
        this.delay = nbt.getInteger("Delay");
        this.maxDelay = nbt.getInteger("MaxDelay");
        this.range = nbt.getInteger("Range");
        this.rangeY = nbt.getInteger("RangeY");
        this.currentAmount = nbt.getInteger("CurrentAmount");
        this.minedAmount = nbt.getInteger("MinedAmount");
        this.maxRange = nbt.getInteger("MaxRange");
        this.maxRangeY = nbt.getInteger("MaxRangeY");
        this.owner = nbt.getString("Owner");
    }

    public void writeCustomNBT(NBTTagCompound nbt) {
        nbt.setInteger("Cooldown", this.cooldown);
        nbt.setInteger("Delay", this.delay);
        nbt.setInteger("MaxDelay", this.maxDelay);
        nbt.setInteger("Range", this.range);
        nbt.setInteger("RangeY", this.rangeY);
        nbt.setInteger("CurrentAmount", this.currentAmount);
        nbt.setInteger("MinedAmount", this.minedAmount);
        nbt.setInteger("MaxRange", this.maxRange);
        nbt.setInteger("MaxRangeY", this.maxRangeY);
        nbt.setString("Owner", this.owner);
        nbt.setString("CustomName", "Mystical Quarry");
    }


    public String checkUpgrade() {
        if (this.worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer) {
            TileEntityUpgradeContainer upgradeContainer = (TileEntityUpgradeContainer) this.worldObj.getTileEntity(this.xCoord, this.yCoord + 1, this.zCoord);
            if (upgradeContainer.hasUpgrade()) {
                return upgradeContainer.getUpgrade();
            } else if (!upgradeContainer.hasUpgrade()) {
                return null;
            }
            return null;
        }
        return null;
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int p_70304_1_) {
        if (this.quarryContents[p_70304_1_] != null) {
            ItemStack itemstack = this.quarryContents[p_70304_1_];
            this.quarryContents[p_70304_1_] = null;
            return itemstack;
        } else {
            return null;
        }
    }

    @Override
    public void setInventorySlotContents(int p_70299_1_, ItemStack p_70299_2_) {
        this.quarryContents[p_70299_1_] = p_70299_2_;

        if (p_70299_2_ != null && p_70299_2_.stackSize > this.getInventoryStackLimit()) {
            p_70299_2_.stackSize = this.getInventoryStackLimit();
        }

        this.markDirty();
    }

    @Override
    public ItemStack getStackInSlot(int p_70301_1_) {
        return this.quarryContents[p_70301_1_];
    }

    @Override
    public ItemStack decrStackSize(int p_70298_1_, int p_70298_2_) {
        if (this.quarryContents[p_70298_1_] != null) {
            ItemStack itemstack;

            if (this.quarryContents[p_70298_1_].stackSize <= p_70298_2_) {
                itemstack = this.quarryContents[p_70298_1_];
                this.quarryContents[p_70298_1_] = null;
                this.markDirty();
                return itemstack;
            } else {
                itemstack = this.quarryContents[p_70298_1_].splitStack(p_70298_2_);

                if (this.quarryContents[p_70298_1_].stackSize == 0) {
                    this.quarryContents[p_70298_1_] = null;
                }

                this.markDirty();
                return itemstack;
            }
        } else {
            return null;
        }
    }

    public String getOwner(){
        return this.owner;
    }

    public void checkForUpgrades() {
        TileEntityUpgradeContainer upgradeContainer = null;
        if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer) {
            upgradeContainer = (TileEntityUpgradeContainer) this.worldObj.getTileEntity(this.xCoord, this.yCoord + 1, this.zCoord);
        }
        for (RecipeUpgradeCreation recipe : RecipeHandler.upgradeCreationRecipes) {
            if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null && upgradeContainer.getUpgrade() == recipe.getName()) {
                if (recipe.getType() == "Speed") {
                    delay = maxDelay / recipe.getModifier();
                } else if (recipe.getType() == "Range") {
                    range = maxRange + recipe.getModifier();
                    rangeY = maxRangeY + (recipe.getModifier() * ((maxRangeY / maxRange) / 2));
                }
            } else if (worldObj.getBlock(this.xCoord, this.yCoord + 1, this.zCoord) == ModBlocks.upgradeContainer && upgradeContainer != null && upgradeContainer.getUpgrade() != recipe.getName()) {
                if (recipe.getType() == "Speed") {
                    delay = maxDelay;
                } else if (recipe.getType() == "Range") {
                    range = maxRange;
                    rangeY = maxRangeY;
                }
            }
        }
    }

    public int getSizeInventory()
    {
        return 9;
    }

    public void func_145976_a(String p_145976_1_)
    {
        this.field_146020_a = p_145976_1_;
    }

    public int getInventoryStackLimit()
    {
        return 64;
    }

    public boolean isUseableByPlayer(EntityPlayer p_70300_1_){        return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : p_70300_1_.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }
    public void openInventory()
    {
        if (this.numPlayersUsing < 0)
        {
            this.numPlayersUsing = 0;
        }

        ++this.numPlayersUsing;
        this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.numPlayersUsing);
        this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, this.getBlockType());
        this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord - 1, this.zCoord, this.getBlockType());
    }

    public void closeInventory()
    {
        if (this.getBlockType() instanceof BlockQuarry)
        {
            --this.numPlayersUsing;
            this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.numPlayersUsing);
            this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, this.getBlockType());
            this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord - 1, this.zCoord, this.getBlockType());
        }
    }

    public boolean isItemValidForSlot(int p_94041_1_, ItemStack p_94041_2_)
    {
        return true;
    }

    public boolean hasCustomInventoryName()
    {
        return this.field_146020_a != null && this.field_146020_a.length() > 0;
    }

    public int getMinedAmount(){
        return this.minedAmount;
    }

    public int getRange(){return this.range;}

    public int getRangeY(){return this.rangeY;}

    public String getSpeed(){return this.delay/20+"spb";}


}

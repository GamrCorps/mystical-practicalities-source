package GamrCorps.mystical.tileentity;

import GamrCorps.mystical.block.ModBlocks;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;

public class TileEntityUpgradeContainer extends TileEntity {
    public static final String name = "TileEntityUpgradeContainer";
    public boolean hasUpgrade = false;
    public String currentUpgrade = "";
    public int currentAmount = 0;

    public String getName() {
        return name;
    }

    public boolean hasUpgrade() {
        if (hasUpgrade) {
            return true;
        } else {
            return false;
        }
    }

    public String getUpgrade() {
        if (hasUpgrade()) {
            return currentUpgrade;
        } else {
            return null;
        }
    }

    public void setUpgrade(String upgrade, int amount) {
        if (hasUpgrade() == true && currentUpgrade == upgrade) {
            incAmount(amount);
        } else if (hasUpgrade() == false) {
            currentUpgrade = upgrade;
            hasUpgrade = true;
            setAmount(amount);
        }
    }

    public void setAmount(int amount) {
        currentAmount = amount;
        if (amount == 0) {
            currentUpgrade = null;
            hasUpgrade = false;
        }
    }

    public void incAmount(int amount) {
        currentAmount = currentAmount + amount;
    }

    public void decAmount(int amount) {
        if (currentAmount > 0) {
            currentAmount = currentAmount - amount;
        }
        if (currentAmount <= 0) {
            currentUpgrade = null;
            hasUpgrade = false;
        }
    }

    public int getAmount() {
        return currentAmount;
    }

    public void sendChatInfoToPlayer(EntityPlayer player) {
        player.addChatComponentMessage(new ChatComponentText("[Upgrade Chamber] Contents: " + this.currentAmount + " " + this.currentUpgrade));
        if (worldObj.getBlock(this.xCoord, this.yCoord - 1, this.zCoord) == ModBlocks.quarry) {
            player.addChatComponentMessage(new ChatComponentText("[Upgrade Chamber] Powering " + worldObj.getBlock(this.xCoord, this.yCoord - 1, this.zCoord).getUnlocalizedName() + " at " + this.xCoord + ", " + (this.yCoord - 1) + ", " + this.zCoord));
        }
    }

    /*
    public boolean hasUpgrade = false;
    public String currentUpgrade = null;
    public int currentAmount = 0;
     */

    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        this.hasUpgrade = nbt.getBoolean("HasUpgrade");
        this.currentUpgrade = nbt.getString("CurrentUpgrade");
        this.currentAmount = nbt.getInteger("CurrentAmount");
    }

    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        nbt.setBoolean("HasUpgrade", this.hasUpgrade);
        nbt.setString("CurrentUpgrade", this.currentUpgrade);
        nbt.setInteger("CurrentAmount", this.currentAmount);
    }


}

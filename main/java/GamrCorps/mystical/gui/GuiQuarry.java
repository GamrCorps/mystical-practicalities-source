package GamrCorps.mystical.gui;

import GamrCorps.mystical.container.ContainerQuarry;
import GamrCorps.mystical.tileentity.TileEntityQuarry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class GuiQuarry extends GuiContainer
{
    private static final ResourceLocation quarryGuiTextures = new ResourceLocation("mysticalmod:textures/gui/quarry.png");
    public TileEntityQuarry tileQuarry;
    private static final String __OBFID = "CL_00000765";

    public GuiQuarry(InventoryPlayer p_i1098_1_, TileEntityQuarry p_i1098_2_)
    {
        super(new ContainerQuarry(p_i1098_1_, p_i1098_2_));
        this.tileQuarry = p_i1098_2_;
    }

    /**
     * Draw the foreground layer for the GuiContainer (everything in front of the items)
     */
    protected void drawGuiContainerForegroundLayer(int p_146979_1_, int p_146979_2_)
    {
        String s = this.tileQuarry.hasCustomInventoryName() ? this.tileQuarry.getInventoryName() : I18n.format(this.tileQuarry.getInventoryName(), new Object[0]);
        this.fontRendererObj.drawString(s, this.xSize / 2 - this.fontRendererObj.getStringWidth(s) / 2, 6, 4210752);
        this.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 8, this.ySize - 96 + 2, 4210752);
        this.fontRendererObj.drawString("Blocks:"+this.tileQuarry.getMinedAmount()+"/"+(this.tileQuarry.pollOres()+this.tileQuarry.getMinedAmount()),2,22,4210752);
        this.fontRendererObj.drawString("Range:"+this.tileQuarry.getRange()+"/"+this.tileQuarry.getRangeY(),2,32,4210752);
        this.fontRendererObj.drawString("Speed:"+this.tileQuarry.getSpeed(),2,42,4210752);
    }

    protected void drawGuiContainerBackgroundLayer(float p_146976_1_, int p_146976_2_, int p_146976_3_)
    {
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(quarryGuiTextures);
        int k = (this.width - this.xSize) / 2;
        int l = (this.height - this.ySize) / 2;
        this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
    }
}
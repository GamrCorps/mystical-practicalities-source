package GamrCorps.mystical.renderer;

import GamrCorps.mystical.item.ModItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;

public class PedestelRenderer extends TileEntitySpecialRenderer {
    ItemStack stack = new ItemStack(ModItems.itemMysticalGlass, 1, 0);
    EntityItem entItem = new EntityItem(Minecraft.getMinecraft().thePlayer.getEntityWorld(), 0D, 0D, 0D, stack);

    @Override
    public void renderTileEntityAt(TileEntity entity, double x, double y, double z, float f) {
        GL11.glPushMatrix();
        //Render Model Here
        GL11.glPopMatrix();
        GL11.glPushMatrix();
        this.entItem.hoverStart = 0.0F;
        RenderItem.renderInFrame = true;
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.02F, (float) z + 0.3F);
        GL11.glRotatef(180, 0, 1, 1);
        RenderManager.instance.renderEntityWithPosYaw(this.entItem, 0.0D, 0.0D, 0.0D, 0.0F, 0.0F);
        RenderItem.renderInFrame = false;
        GL11.glPopMatrix();
    }
}
package GamrCorps.mystical.commands;

import GamrCorps.mystical.handler.ManaNetworkHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

import java.util.ArrayList;
import java.util.List;

public class ManaCommand implements ICommand {
    private List aliases;

    public ManaCommand() {
        this.aliases = new ArrayList();
        this.aliases.add("mana");
    }

    @Override
    public String getCommandName() {
        return "mana";
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender) {
        return "mana <add/remove/set> <amount> <player>";
    }

    @Override
    public List getCommandAliases() {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender icommandsender, String[] astring) {
        if (astring.length == 0 || astring.length == 1 || astring.length == 2) {
            icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
            return;
        } else if (astring[0] == "add") {
            ManaNetworkHandler.setCurrentEssence(astring[2], ManaNetworkHandler.getCurrentEssence(astring[2]) + Integer.parseInt(astring[1]));
            icommandsender.addChatMessage(new ChatComponentText("Added " + astring[1] + " mana to " + astring[2] + "."));
        } else if (astring[0] == "remove") {
            ManaNetworkHandler.setCurrentEssence(astring[2], ManaNetworkHandler.getCurrentEssence(astring[2]) - Integer.parseInt(astring[1]));
            icommandsender.addChatMessage(new ChatComponentText("Removed " + astring[1] + " mana from " + astring[2] + "."));
        } else if (astring[0] == "set") {
            ManaNetworkHandler.setCurrentEssence(astring[2], Integer.parseInt(astring[1]));
            icommandsender.addChatMessage(new ChatComponentText("Set " + astring[2] + "'s mana to " + astring[1] + "."));
        }

    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender icommandsender) {
        return true;
    }

    @Override
    public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring) {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] astring, int i) {
        return false;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
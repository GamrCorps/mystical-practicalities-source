package GamrCorps.mystical.functions;


import GamrCorps.mystical.block.ModBlocks;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class CoalTreeGenFunctions {
    public static void generateCoalTree(World world, int i, int j, int k) {
        world.setBlock(i, j + 1, k, Blocks.coal_block, 0, 2);
        world.setBlock(i, j, k, Blocks.coal_block, 0, 2);
        world.setBlock(i, j + 1, k + 1, ModBlocks.coalLeaves, 0, 2);
        world.setBlock(i, j + 1, k - 1, ModBlocks.coalLeaves, 0, 2);
        world.setBlock(i + 1, j + 1, k, ModBlocks.coalLeaves, 0, 2);
        world.setBlock(i - 1, j + 1, k, ModBlocks.coalLeaves, 0, 2);
        world.setBlock(i, j + 2, k, ModBlocks.coalLeaves, 0, 2);
    }
}

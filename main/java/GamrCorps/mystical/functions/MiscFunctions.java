package GamrCorps.mystical.functions;

import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class MiscFunctions {
    public static int randNumInRange(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }

    public boolean isItemOre(ItemStack itemStack){
        if (itemStack.getItem().getUnlocalizedName().toLowerCase().contains("ore") || itemStack.getItem() == Item.getItemFromBlock(Blocks.quartz_ore) || itemStack.getItem() == Item.getItemFromBlock(Blocks.lapis_ore)){
            return true;
        }
        return false;
    }
}

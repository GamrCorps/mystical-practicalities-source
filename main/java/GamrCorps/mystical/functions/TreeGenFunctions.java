package GamrCorps.mystical.functions;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class TreeGenFunctions {
    public static void generateTree(String type, World world, Block logBlock, Block leavesBlock, int i, int j, int k) {
        if (type != null) {
            if (type == "oak") {
                generateOakTree(world, logBlock, leavesBlock, i, j, k);
            }
        }
    }

    public static void generateOakTree(World world, Block logBlock, Block leavesBlock, int i, int j, int k) {
        int trunkHeight = MiscFunctions.randNumInRange(4, 6);
        //Trunk
        for (int l = 0; l <= trunkHeight; l++) {
            world.setBlock(i, j + l, k, logBlock, 0, 2);
        }
        //Bottom 2 layers of leaves
        for (int l = 0; l <= 1; l++) {
            for (int m = -2; m <= 2; m++) {
                for (int n = -2; n <= 2; n++) {
                    if (world.getBlock(i + m, j + (trunkHeight - 2) + l, k + n) == Blocks.air) {
                        if (Math.abs(m) + Math.abs(n) == 4) {
                            if (MiscFunctions.randNumInRange(0, 3) == 2) {
                                world.setBlock(i + m, j + (trunkHeight - 2) + l, k + n, leavesBlock, 0, 2);
                            }
                        } else {
                            world.setBlock(i + m, j + (trunkHeight - 2) + l, k + n, leavesBlock, 0, 2);
                        }
                    }
                }
            }
        }
        //Second Layer
        for (int m = -1; m <= 1; m++) {
            for (int n = -1; n <= 1; n++) {
                if (world.getBlock(i + m, j + (trunkHeight - 3), k + n) == Blocks.air) {
                    if (Math.abs(m) + Math.abs(n) == 2) {
                        if (MiscFunctions.randNumInRange(0, 3) == 2) {
                            world.setBlock(i + m, j + trunkHeight, k + n, leavesBlock, 0, 2);
                        }
                    } else {
                        world.setBlock(i + m, j + trunkHeight, k + n, leavesBlock, 0, 2);
                    }
                }
            }
        }
        //Top Layer
        if (world.getBlock(i + 1, j + trunkHeight + 1, k) == Blocks.air) {
            world.setBlock(i + 1, j + trunkHeight + 1, k, leavesBlock, 0, 2);
        }
        if (world.getBlock(i - 1, j + trunkHeight + 1, k) == Blocks.air) {
            world.setBlock(i - 1, j + trunkHeight + 1, k, leavesBlock, 0, 2);
        }
        if (world.getBlock(i, j + trunkHeight + 1, k + 1) == Blocks.air) {
            world.setBlock(i, j + trunkHeight + 1, k + 1, leavesBlock, 0, 2);
        }
        if (world.getBlock(i, j + trunkHeight + 1, k - 1) == Blocks.air) {
            world.setBlock(i, j + trunkHeight + 1, k - 1, leavesBlock, 0, 2);
        }
        if (world.getBlock(i, j + trunkHeight + 1, k) == Blocks.air) {
            world.setBlock(i, j + trunkHeight + 1, k, leavesBlock, 0, 2);
        }
    }
}